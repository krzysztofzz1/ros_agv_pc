import cv2
import numpy as np
import depthai as dai
import struct
import time
import xml.etree.ElementTree as ET
import math as math
import rclpy
from rclpy.node import Node
from interfaces.msg import Steer

def find_yellow_tape(image):
    # Convert image to HSV color space
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Yellow color range in HSV space
    lower_yellow = np.array([0, 90, 90])
    upper_yellow = np.array([40, 255, 255])

    # Threshold the image to detect yellow tape
    mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

    # Find contours on the mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if contours:
        # Select the largest contour (can change this criterion)
        largest_contour = max(contours, key=cv2.contourArea)
        
        # Calculate the bounding rectangle of the contour
        x, y, w, h = cv2.boundingRect(largest_contour)

        # Find the center of the rectangle
        center_x = x + w // 2
        center_y = y 

        # Draw a rectangle around the contour on the original image
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        return center_x, center_y
    else:
        return None

def calculate_deviation(image_width, image_height, center_x, center_y):
    # Calculate deviation from the camera center
    deviation_x = center_x - image_width // 2
    deviation_y =  - center_y + image_height
    return deviation_x*pixel, deviation_y*pixel



class agv_command:
   
    pos = math.nan
    trq = 0.0
    acc = math.nan

    def __init__(self, type, id, mode, var_1, var_2, var_3):
        self.type = type
        self.id = id
        self.mode = mode
        if(type == 0x01):
            self.vel = var_1
        elif(type == 0x00):
            self.x_v = var_1
            self.y_v = var_2
            self.rot_v = var_3

    def get_bytes(self):
        ba_pos = bytearray(struct.pack("f", self.pos))
        ba_arb = bytearray([self.type,self.id,self.mode])
        if(self.type == 0x01):  
            ba_vel = bytearray(struct.pack("f", self.vel))
            ba_trq = bytearray(struct.pack("f", self.trq))
            ba_acc = bytearray(struct.pack("f", self.acc))
            byte_array = ba_arb+ba_pos+ba_vel+ba_trq+ba_acc

        elif(self.type == 0x00): 
            ba_xv = bytearray(struct.pack("f", self.x_v))
            ba_yv = bytearray(struct.pack("f", self.y_v))
            ba_rv = bytearray(struct.pack("f", self.rot_v))
            ba_nop = bytearray(4*[0x50])
            byte_array = ba_arb+ba_xv+ba_yv+ba_rv+ba_nop
            
        return byte_array 
    
def send_to_stm(to_send: agv_command):
    bytes_to_send = to_send.get_bytes()
    print(bytes_to_send)
    # ser.write(bytes_to_send) #if you want to directly send to STM
    msg = Steer()
    msg.data = bytes_to_send
    publisher.publisher_.publish(msg)
    # publisher.get_logger().info('Publishing: ' + str(msg.data)) #works but fucks shit up

    
class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('tui_publisher')
        self.publisher_ = self.create_publisher(Steer, 'tui_steering', 10)

class AGV:
    member=0.0
    def calculate(deviation):
            if deviation[1] >= AGV.member:
                AGV.move_forward(deviation[0], deviation[1])
                AGV.member=deviation[0]
            elif deviation[0] > 0.0:
                AGV.move_right()
            elif deviation[0] < 0.0:
                AGV.move_left()
            else:
                AGV.stop()
                

    def move_forward(deviation_x, deviation_y):
        speed = 0.1
        camera_holder_distance = 0.135
        AGV_center_y_distance = deviation_y + 0.44 + camera_holder_distance
        AGV_center_distance = math.sqrt(AGV_center_y_distance * AGV_center_y_distance + deviation_x * deviation_x)
        AGV_control_vector = [0, 0 , 0]
        AGV_control_vector[0] =  speed * deviation_x/AGV_center_distance
        AGV_control_vector[1]= speed * AGV_center_y_distance / AGV_center_distance
        AGV_control_vector[2] = -math.asin(deviation_x/AGV_center_distance)
        AGV.send(AGV_control_vector)

    def move_right():

        initial_speed=0.1
        initial_ramp=5
        jump = initial_speed/initial_ramp
        AGV_control_vector = [0, 0 , 0]

        for i in range (initial_ramp, 0, -1):
            AGV_control_vector[0]=0.0
            AGV_control_vector[1]=initial_speed
            AGV_control_vector[2]=0.0
            initial_speed=initial_speed-jump
            AGV.send(AGV_control_vector)

        AGV_control_vector[0]=0.0
        AGV_control_vector[1]=0.0
        AGV_control_vector[2]=1.570796326795
        AGV.send(AGV_control_vector)


    def move_left():
        
        initial_speed=0.1
        initial_ramp=5
        jump = initial_speed/initial_ramp
        AGV_control_vector = [0, 0 , 0]

        for i in range (initial_ramp, 0, -1):
            AGV_control_vector[0]=0.0
            AGV_control_vector[1]=initial_speed
            AGV_control_vector[2]=0.0,
            initial_speed=initial_speed-jump
            time.sleep(0.1)
            AGV.send(AGV_control_vector)

        AGV_control_vector[0]=0.0
        AGV_control_vector[1]=0.0
        AGV_control_vector[2]=-1.570796326795
        time.sleep(0.1)
        AGV.send(AGV_control_vector)


    def stop():
        AGV_control_vector = [0, 0 , 0]
        AGV_control_vector[0]=0.0
        AGV_control_vector[1]=0.0
        AGV_control_vector[2]=0.0
        mode = 0x04
        new_command = agv_command(0x00, 0x10, mode, AGV_control_vector[0], AGV_control_vector[1], AGV_control_vector[2])
        send_to_stm(new_command)


    def send(AGV_control_vector):
        mode = 0x01
        new_command = agv_command(0x00, 0x10, mode, AGV_control_vector[0], AGV_control_vector[1], AGV_control_vector[2])
        time.sleep(0.1)
        send_to_stm(new_command)
        


def calibration():
    tree = ET.parse('calib.xml')
    callibration = tree.getroot()
    for center in callibration.findall('center'):
        x1 = center.get('x1')
        y1 = center.get('y1')
        x2 = center.get('x2')
        y2 = center.get('y2')
        x3 = center.get('x3')
        y3 = center.get('y3')
        x4 = center.get('x4')
        y4 = center.get('y4')
            
    pts = np.float32([[x1,y1], [x2,y2], [x3,y3], [x4,y4]])
       
    return pts
pixel=0.1/500
pts1 = np.array(calibration())
pts2 = np.float32([[0,0], [500,0], [0,500], [500,500]])
matrix=cv2.getPerspectiveTransform(pts1, pts2)
# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
Preview = pipeline.create(dai.node.XLinkOut)

Preview.setStreamName("preview")
# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(Preview.input)
   

def main():
    rclpy.init()
    global publisher
    publisher = MinimalPublisher()
    robot=AGV
    # Use the DepthAI device with the defined pipeline
    with dai.Device(pipeline) as device:
        # Get the output queue for preview frames
        preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
        # Continuous loop for processing frames
        while True:
            # Get the latest preview frame
            inPreview = preview.get()

            # Check if the frame is not empty
            if inPreview is not None:
                # Get the frame from the preview
                frame = inPreview.getCvFrame()
                transform_frame = frame.copy()

                # Perform perspective transformation on the frame
                processed_frame = cv2.warpPerspective(transform_frame, matrix, (500,500))
                
                # Detect the yellow tape in the processed frame
                tape_center = find_yellow_tape(processed_frame)

                # If yellow tape is detected
                if tape_center:
                    # Calculate deviation from the camera center
                    (image_height, image_width) = processed_frame.shape[:2]
                    deviation = calculate_deviation(image_width, image_height, tape_center[0], tape_center[1])

                    robot.calculate(deviation)

                    # Lower the camera center point by 60 pixels
                    center_y_camera = processed_frame.shape[0] // 2 + 160

                    # Display deviation information on the frame
                    cv2.putText(processed_frame, f"Deviation: {deviation}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)

                    # Display camera center point after lowering by 60 pixels
                    cv2.circle(processed_frame, (image_width // 2, center_y_camera), 5, (255, 0, 0), -1)

                    # Display center of the rectangle (tape) on the frame
                    cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

                # Display original frame and processed frame with yellow tape marked
                cv2.imshow("Original Frame", frame)
                cv2.imshow("Processed Frame", processed_frame)

            # Break the loop if 'q' key is pressed
            if cv2.waitKey(1) == ord('q'):
                break


if __name__ == '__main__':
    main()
