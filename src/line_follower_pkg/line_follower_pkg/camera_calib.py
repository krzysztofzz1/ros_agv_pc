import cv2
import numpy as np
import depthai as dai
import xml.etree.ElementTree as ET

def find_color(image, lower_color, upper_color):
    # Convert image to HSV color space
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # Create a mask using the specified color bounds
    mask = cv2.inRange(hsv, lower_color, upper_color)
    # Find contours in the mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    if contours:
        # Get the largest contour
        largest_contour = max(contours, key=cv2.contourArea)
        # Get bounding rectangle of the largest contour
        x, y, w, h = cv2.boundingRect(largest_contour)
        # Calculate center of the bounding rectangle
        center_x = x + w // 2
        center_y = y + h // 2
        # Draw rectangle around the contour on the image
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return center_x, center_y
    else:
        return None

# Define lower and upper color bounds
lower_mask = np.array([0, 0, 0])
upper_mask = np.array([150, 150, 150])

# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
Preview = pipeline.create(dai.node.XLinkOut)
Preview.setStreamName("preview")

# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(Preview.input)

def main():
    # Connect to device and start pipeline
    with dai.Device(pipeline) as device:
        preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
        
        yellow_x, yellow_y = 0, 0
        red_x, red_y = 0, 0
        blue_x, blue_y = 0, 0
        green_x, green_y = 0, 0
        y = 0
        b = 0
        r = 0
        g = 0
        
        while True:
            inPreview = preview.get()

            if inPreview is not None:
                frame = inPreview.getCvFrame()
                processed_frame = frame.copy()
                top_left = processed_frame[0:250, 0:250]
                top_right = processed_frame[0:250, 250:500]
                bottom_left = processed_frame[250:500, 0:250]
                bottom_right = processed_frame[250:500, 250:500]
                # Find yellow color
                tape_center= find_color(top_right, lower_mask, upper_mask)
                if tape_center:
                    tape_x=tape_center[0] + 250
                    tape_y=tape_center[1]
                    yellow_x += tape_x
                    yellow_y += tape_y
                    y += 1
                    cv2.circle(processed_frame, (tape_x, tape_y), 5, (0, 0, 255), -1)

                # Find blue color
                tape_center= find_color(top_left, lower_mask, upper_mask)
                if tape_center:
                    tape_x=tape_center[0]
                    tape_y=tape_center[1]
                    blue_x += tape_x
                    blue_y += tape_y
                    b += 1
                    cv2.circle(processed_frame, (tape_x, tape_y), 5, (0, 0, 255), -1)

                # Find green color
                tape_center = find_color(bottom_left, lower_mask, upper_mask)
                if tape_center:
                    tape_x=tape_center[0]
                    tape_y=tape_center[1] + 250
                    green_x += tape_x
                    green_y += tape_y
                    g += 1
                    cv2.circle(processed_frame, (tape_x, tape_y), 5, (0, 0, 255), -1)

                # Find red color
                tape_center = find_color(bottom_right, lower_mask, upper_mask)
                if tape_center:
                    tape_x=tape_center[0]+250
                    tape_y=tape_center[1]+250
                    red_x += tape_x
                    red_y += tape_y
                    r += 1
                    cv2.circle(processed_frame, (tape_x, tape_y), 5, (0, 0, 255), -1)

                cv2.imshow("processed_frame Frame", processed_frame)
        
            if cv2.waitKey(1) == ord('q'):
                break

        # Calculate average centers
        if y != 0:
            yellow_x /= y
            yellow_y /= y
        if b != 0:
            blue_x /= b
            blue_y /= b
        if g != 0:
            green_x /= g
            green_y /= g
        if r != 0:
            red_x /= r
            red_y /= r
            
        root = ET.Element('root')

        print("Blue center:", yellow_x, yellow_y)
        print("Blue center:", blue_x, blue_y)
        print("Green center:", green_x, green_y)
        print("Red center:", red_x, red_y)
        
        # Adding data to XML tree
        element = ET.SubElement(root, "center")
        
        element.set("x1", str(blue_x))
        element.set("y1", str(blue_y))
        element.set("x2", str(yellow_x))
        element.set("y2", str(yellow_y))
        element.set("x3", str(green_x))
        element.set("y3", str(green_y))
        element.set("x4", str(red_x))
        element.set("y4", str(red_y))

        # Creating XML tree
        tree = ET.ElementTree(root)

        # Writing tree to XML file
        tree.write("calib.xml")

    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()